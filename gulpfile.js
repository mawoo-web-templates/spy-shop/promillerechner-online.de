var gulp = require('gulp');
var fs = require('fs');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var rev = require('gulp-rev');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var tinypng = require('gulp-tinypng-compress');
var htmlmin = require('gulp-htmlmin');
var jsonminify = require('gulp-jsonminify');
var inject = require('gulp-inject');
var clean = require('gulp-clean');
var jsonCss;

gulp.task('delete-css-cache-files', function() {
  return gulp
    .src(['./dist/**/*.css', './dist/**/*.map'], {
      read: false
    })
    .pipe(clean());
});

gulp.task('sass', ['delete-css-cache-files'], function() {
  return (
    gulp
      .src('./src/sass/**/*.scss')
      //.pipe(sourcemaps.init())
      .pipe(
        sass({
          // nested, expanded, compact, compressed
          outputStyle: 'compressed'
        }).on('error', sass.logError)
      )
      //.pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./dist/css'))
  );
});

gulp.task('css-hash', ['sass'], function() {
  return gulp
    .src('./dist/css/styles.css')
    .pipe(gulp.dest('./dist/css'))
    .pipe(rev())
    .pipe(gulp.dest('./dist/css'))
    .pipe(rev.manifest())
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('inject-css', ['css-hash'], function() {
  jsonCss = JSON.parse(fs.readFileSync('./dist/css/rev-manifest.json'));
  var target = gulp.src('./src/**/*.html');
  var source = gulp.src('./dist/css/' + jsonCss['styles.css'], {
    read: false
  });

  return target
    .pipe(
      inject(source, {
        ignorePath: 'dist'
      })
    )
    .pipe(gulp.dest('./src'));
});

gulp.task('js', function() {
  return gulp
    .src('./src/js/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('copy-img-tinypng', function() {
  return gulp
    .src('./src/img/**/*.{png,jpg,jpeg}')
    .pipe(
      tinypng({
        key: '6_2Zvi5VbHQSrJKPfShhwUz5CjDdD97l',
        sigFile: './src/img/.tinypng-sigs',
        log: true
      })
    )
    .pipe(gulp.dest('./dist/img'));
});

gulp.task('copy-html', ['inject-css'], function() {
  return gulp
    .src('./src/**/*.html')
    .pipe(
      htmlmin({
        collapseWhitespace: true,
        minifyJS: true,
        removeComments: true
      })
    )
    .pipe(gulp.dest('./dist'));
});

gulp.task('copy-files', function() {
  var copy = {
    files: ['./src/*.ico', './src/*.png', './src/.htaccess']
  };
  return gulp
    .src(copy.files, {
      base: './src/'
    })
    .pipe(gulp.dest('./dist'));
});

gulp.task('copy-json', function() {
  var copy = {
    files: ['./src/data/**/*']
  };
  return gulp
    .src(copy.files, {
      base: './src/'
    })
    .pipe(jsonminify())
    .pipe(gulp.dest('./dist'));
});

gulp.task('build', ['js', 'copy-img-tinypng', 'copy-html', 'copy-files', 'copy-json'], function() {
  gulp.watch('./src/sass/**/*.scss', ['copy-html']);
  gulp.watch('./src/js/**/*.js', ['js']);
  gulp.watch('./src/**/*.html', ['copy-html']);
  gulp.watch('./src/**/*.json', ['copy-json']);
});
