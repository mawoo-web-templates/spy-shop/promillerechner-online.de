(function ($) {
    $(document).ready(function () {
        var cookie = {
            set: function (cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = 'expires=' + d.toGMTString();
                document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
            },
            get: function (cname) {
                var name = cname + '=';
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return '';
            },
            check: function () {
                var cookieVal = this.get('cookie_notice');
                console.log(cookieVal);
                if (cookieVal !== 'disabled') {
                    this.notice();
                }
            },
            notice: function () {
                var link = 'datenschutzerklarung.html';
                $('body').append('<div id="cookie-notice" class="cookie-notice"><p>Cookies helfen uns bei der Bereitstellung unserer Dienste. Indem Sie mit der Nutzung unserer Webseite www.promillerechner-online.de fortfahren, stimmen Sie der Verwendung unserer Cookies zu. Sie können jedoch jederzeit Ihre Cookie-Einstellungen ändern. Nähere Informationen zu Cookies finden Sie <a href="' + link + '">hier</a>.</p><span class="icon">i</span><span class="close-cookie-notice"></span></div>');
            }
        };

        cookie.check();

        $('body').on('click', '.close-cookie-notice', function () {
            cookie.set('cookie_notice', 'disabled', 30);
            $('#cookie-notice').addClass('disabled');
        });
    });
}(jQuery));